# How To Use

You need

* A PGP keypair. You should use a keypair which is used for Keepass files, only.
* A .env file


```
KEYPASS_PGP_KEY="0123456789ABCDEF0123456789ABCDEF01234567"
NEXTCLOUD_URL="https://nextcloud.example.com/remote.php/dav/files/keepassuser/keepass/keepass.asc"
NEXTCLOUD_USER="keepassuser"
NEXTCLOUD_PASSWORD="PasswordXYZ1"
KEEPASS_DIR="/home/keepassuser/Dokumente/keepass"
```

This is configured for a Nextcloud instance.

